import SgVideoLightbox from './Modules/sgVideoLightbox';
import SgVideo from './Modules/sgVideo';

/* eslint no-new: "off" */
function main() {
	new SgVideoLightbox();
	SgVideo.initDefault();
}

// Loading hasn't finished yet
if (document.readyState === 'loading') {
	document.addEventListener('DOMContentLoaded', main);
} else {
	// DOMContentLoaded has already fired
	main();
}
