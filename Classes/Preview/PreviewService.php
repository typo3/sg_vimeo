<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgVimeo\Preview;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * PreviewService, to get Views while we have to duplicate previewCode
 */
class PreviewService {
	public const RETURNTYPE_ARR = 'array';

	public function getPluginView(array $row): StandaloneView {
		/** @var StandaloneView $view */
		$view = GeneralUtility::makeInstance(StandaloneView::class);
		$view->setPartialRootPaths(['EXT:sg_vimeo/Resources/Private/Partials/Backend']);
		$view->setTemplateRootPaths(['EXT:sg_vimeo/Resources/Private/Templates/Vimeo']);
		$view->assign('uid', $row['uid']);

		// Get available plugin settings and their values from flexform
		$pluginConfiguration = GeneralUtility::xml2array(
			$row['pi_flexform'],
			'T3DataStructure'
		)['data']['sDEF']['lDEF'];

		$templateData = [
			'vimeoId' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.id'),
			'maxResults' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.maxResults'),
			'showTitle' => (int) ($this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.showTitle') ?? 1),
			'showDescription' => (int) ($this->passVDefOnKeyToTemplate(
				$pluginConfiguration,
				'settings.showDescription'
			) ?? 1),
			'disableLightbox' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.disableLightbox'),
			'disableLightboxMobile' => $this->passVDefOnKeyToTemplate(
				$pluginConfiguration,
				'settings.disableLightboxMobile'
			),
			'aspectRatio' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.aspectRatio'),
			'thumbnailType' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.thumbnailType'),
			'thumbnailImagesCount' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.thumbnailImages'),
			'showApiResult' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.showApiResult'),
			'urlParameters' => $this->passVDefOnKeyToTemplate($pluginConfiguration, 'settings.urlParameters'),
			'header' => $row['header'],
		];

		$view->assign('data', $templateData);
		$view->assign(
			'headerLabel',
			BackendUtility::getLabelFromItemListMerged(
				$row['pid'],
				'tt_content',
				'list_type',
				$row['list_type'],
				$row
			)
		);
		return $view;
	}

	/**
	 * @param array $conf
	 * @param string $key
	 * @param string $returnType
	 * @return array|mixed|string
	 */
	private function passVDefOnKeyToTemplate(array $conf, string $key, string $returnType = '') {
		if (isset($conf[$key])) {
			return $conf[$key]['vDEF'];
		}

		// check if we got a possible returntype:
		if ($returnType === self::RETURNTYPE_ARR) {
			return [];
		}

		return '';
	}
}
