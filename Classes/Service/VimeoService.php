<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgVimeo\Service;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use SGalinski\SgVimeo\Exception\RegionRestrictedException;
use SGalinski\SgVimeo\Filter\FilterParameterBag;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use Vimeo\Exceptions\VimeoRequestException;
use Vimeo\Vimeo;

// Fallback to the PHAR archive containing the dependency on the vimeo API in case
// this is a legacy installation - not in composer mode
try {
	if (!class_exists('\Vimeo\Exceptions\VimeoRequestException')) {
		require_once 'phar://' . ExtensionManagementUtility::extPath('sg_vimeo') .
			'Libraries/vimeo-api.phar/autoload.php';
	}
} catch (\Exception $e) {
	require_once 'phar://' . ExtensionManagementUtility::extPath('sg_vimeo') .
		'Libraries/vimeo-api.phar/autoload.php';
}

/**
 * Vimeo Helper Service
 */
class VimeoService implements LoggerAwareInterface {
	use LoggerAwareTrait;

	protected const API_CHANNEL = '/channels/';
	protected const API_VIDEO = '/videos/';
	protected const API_SHOWCASE = '/me/albums/';

	/**
	 * https://developer.vimeo.com/api/authentication#supported-scopes
	 */
	protected const SCOPE = 'public';

	public const CACHE_LIFETIME_IN_SECONDS = 86400;

	/**
	 * @var FrontendInterface
	 */
	protected FrontendInterface $cache;

	/**
	 * @var Vimeo
	 */
	protected Vimeo $vimeoApiClient;

	/**
	 * @var array
	 */
	protected array $paginatedResponseData;

	/**
	 * @var int
	 * Used for the `per_page` param, for the vimeo API requests.
	 * Use `per_page` to set the number of representations per page from the default value of 25 up to a maximum value of 100.
	 */
	protected int $maxResultsPerPage = 25;

	/**
	 * @var int
	 * The amount of videos fetched within the current pagination request
	 */
	protected int $amountOfVideosFetched = 0;

	/**
	 * VimeoService constructor.
	 *
	 * @param string $clientId
	 * @param string $clientSecret
	 * @param string $personalAccessToken
	 * @param FrontendInterface $cache
	 */
	public function __construct(
		string $clientId,
		string $clientSecret,
		string $personalAccessToken,
		FrontendInterface $cache
	) {
		$this->vimeoApiClient = new Vimeo($clientId, $clientSecret, $personalAccessToken);
		// We only need to request an unauthenticated token, if there is no personal access token provided already.
		// An authenticated access token with the public scope is identical to an unauthenticated access token,
		// except that you can use the /me endpoint to refer to the currently logged-in user.
		// Accessing /me with an unauthenticated access token generates an error.
		// See also: https://developer.vimeo.com/api/authentication
		if ($personalAccessToken === '') {
			$this->requestAccessToken();
		}
		$this->cache = $cache;
	}

	/**
	 * @param FilterParameterBag $parameterBag
	 * @return array|null
	 * @throws VimeoRequestException
	 */
	public function getVimeoData(FilterParameterBag $parameterBag): ?array {
		$maxResults = $parameterBag->get('maxResults');
		$vimeoId = $parameterBag->get('id');
		$queryString = $parameterBag->get('queryString');
		$filters = $parameterBag->getFilterInstances();
		if ($queryString) {
			$parameterBag->set('query', $queryString);
		}

		$filterValues = [];
		foreach ($filters as $filter) {
			$filterValues[] = $filter->getFilterValues();
		}
		$filtersHash = md5(json_encode($filterValues));

		$response = [];
		$this->maxResultsPerPage = $maxResults;
		$cacheKey = 'sg_vimeo' . sha1($vimeoId . $maxResults . $queryString . $filtersHash);
		$disableVimeoCache = $parameterBag->getDisableCache();
		if (!$disableVimeoCache) {
			$cachedResult = $this->cache->get($cacheKey);
			if ($cachedResult) {
				return $cachedResult;
			}
		}

		if (str_starts_with($vimeoId, 'showcase')) {
			$showcaseId = explode('/', $vimeoId)[1];
			$response['items'] = $this->addVideoIdsToResponse(
				$this->getShowcaseVideos((int) $showcaseId, $parameterBag)
			);
			$response['kind'] = 'showcase';
		} elseif (str_starts_with($vimeoId, 'channel')) {
			$channelId = explode('/', $vimeoId)[1];
			$response['items'] = $this->addVideoIdsToResponse($this->getChannelVideos($channelId, $parameterBag));
			$response['kind'] = 'channel';
		} else {
			$response['items'] = $this->addVideoIdsToResponse($this->getVideo((int) $vimeoId, $parameterBag));
			$response['kind'] = 'video';
		}

		foreach ($filters as $filter) {
			$filter->modifyResponse($response);
		}

		if (!$disableVimeoCache) {
			$this->cache->set($cacheKey, $response, [], self::CACHE_LIFETIME_IN_SECONDS);
		}

		return $response;
	}

	/**
	 * Extracts the video id from the video's canonical relative URI and adds it to each entry with the key 'videoId'
	 *
	 * @param array $response
	 * @return array
	 */
	protected function addVideoIdsToResponse(array $response): array {
		if (empty($response)) {
			return [];
		}

		foreach ($response as $index => $item) {
			if (array_key_exists('uri', $item)) {
				$uri = $item['uri'];
				$videoId = (int) str_replace('/videos/', '', $uri);
				$response[$index]['videoId'] = $videoId;
			}
		}

		return $response;
	}

	/**
	 * Unauthenticated API requests must generate an access token.
	 * (Access tokens without a user. These tokens can view only public data.)
	 * You should not generate a new access token for each request.
	 * Instead, request an access token once and use it forever.
	 */
	protected function requestAccessToken(): void {
		$token = $this->vimeoApiClient->clientCredentials(self::SCOPE);
		if (isset($token['body']['access_token'])) {
			$this->vimeoApiClient->setToken($token['body']['access_token']);
		}
	}

	/**
	 * Returns the response body, wrapped in an array if the response contains a single item.
	 * If the response is a paginated response, all items are fetched until the maxResultsPerPage is reached,
	 * or no $nextUrl is available anymore (last page reached). Since the flexform allows a max value of 100 currently,
	 * this function will never go past the first page, since the vimeo API allows a value of 100 as max value for the `per_page` parameter.
	 *
	 * @param array|null $response
	 * @return array
	 */
	protected function preprocessApiResponse(?array $response): array {
		if (!is_array($response) || $response['status'] >= 400) {
			return [];
		}

		// log error & return here, since the response was not OK
		if ($response['status'] !== 200) {
			$this->logger->error('sg_vimeo API Request failed, got the following response:', $response);
			return [$response['body']];
		}

		// @TODO: we could check $response['headers‘]['X-RateLimit-Remaining'] here for remaining quota
		if (array_key_exists('paging', $response['body'])) {
			$amountOfVideosInResponse = is_countable($response['body']['data']) ? count($response['body']['data']) : 0;
			$this->amountOfVideosFetched += $amountOfVideosInResponse;
			$this->paginatedResponseData[] = $response['body']['data'];
			$nextUrl = $response['body']['paging']['next'];
			if ($this->amountOfVideosFetched >= $this->maxResultsPerPage || $nextUrl === NULL) {
				// return flattened array here, so that we don't end up with one sub array per pagination page
				return array_merge(...$this->paginatedResponseData);
			}

			$this->fetchPaginatedResult($nextUrl);
		}

		// wrap response body in an array, so that we can treat all return values the same in the template
		return [$response['body']];
	}

	/**
	 * @param string $nextUrl
	 * @return array|null
	 */
	protected function fetchPaginatedResult(string $nextUrl): ?array {
		try {
			$response = $this->vimeoApiClient->request($nextUrl);
		} catch (VimeoRequestException $e) {
			return NULL;
		}

		return $this->preprocessApiResponse($response);
	}

	/**
	 * Returns a single video for the given $videoId
	 *
	 * @see https://developer.vimeo.com/api/reference/videos#get_video
	 * @param int $videoId
	 * @param FilterParameterBag $filterParameterBag
	 * @return array|null
	 * @throws VimeoRequestException
	 */
	public function getVideo(int $videoId, FilterParameterBag $filterParameterBag): ?array {
		// use field filtering, to save on quota, see: https://developer.vimeo.com/guidelines/rate-limiting
		$fieldsToSelect = 'uri,name,description,link,embed,pictures,release_time,width,height';

		$parameters = [];
		$parameters['fields'] = $fieldsToSelect;

		foreach ($filterParameterBag->getFilterInstances() as $filter) {
			$filter->modifyRequest($parameters);
		}

		$query = http_build_query($parameters);

		try {
			$response = $this->vimeoApiClient->request(self::API_VIDEO . $videoId . '?' . $query);
		} catch (VimeoRequestException $e) {
			throw $e;
		}

		return $this->preprocessApiResponse($response);
	}

	/**
	 * Returns all videos for the given $channelIdentifier
	 *
	 * @see https://developer.vimeo.com/api/reference/channels#get_channel_videos
	 * @param string $channelIdentifier
	 * @param FilterParameterBag $filterParameterBag
	 * @return array|null
	 * @throws VimeoRequestException
	 */
	public function getChannelVideos(string $channelIdentifier, FilterParameterBag $filterParameterBag): ?array {
		// use field filtering, to save on quota, see: https://developer.vimeo.com/guidelines/rate-limiting
		$fieldsToSelect = 'uri,name,description,link,embed,pictures,release_time,width,height';

		$parameters = [
			'fields' => $fieldsToSelect,
			'sort' => 'default',
			'per_page' => $this->maxResultsPerPage
		];

		foreach ($filterParameterBag->getFilterInstances() as $filter) {
			$filter->modifyRequest($parameters);
		}

		$query = http_build_query($parameters);
		$response = $this->vimeoApiClient->request(
			self::API_CHANNEL . $channelIdentifier . self::API_VIDEO . '?' . $query
		);
		if ($response['status'] === 404 && ($response['body']['error'] ?? '') === 'This resource is restricted in your region.') {
			throw new RegionRestrictedException($response['body']['error']);
		}

		return $this->preprocessApiResponse($response);
	}

	/**
	 * Returns all videos for the given $showcaseId
	 *
	 * @see https://developer.vimeo.com/api/reference/showcases#get_showcase
	 * @param string $showcaseId
	 * @param FilterParameterBag $filterParameterBag
	 * @return array|null
	 * @throws VimeoRequestException
	 */
	public function getShowcaseVideos(string $showcaseId, FilterParameterBag $filterParameterBag): ?array {
		// use field filtering, to save on quota, see: https://developer.vimeo.com/guidelines/rate-limiting
		$fieldsToSelect = 'uri,name,description,link,embed,pictures,release_time,width,height';

		$parameters = [
			'fields' => $fieldsToSelect,
			'sort' => 'default',
			'per_page' => $this->maxResultsPerPage
		];

		foreach ($filterParameterBag->getFilterInstances() as $filter) {
			$filter->modifyRequest($parameters);
		}

		$query = http_build_query($parameters);
		$response = $this->vimeoApiClient->request(
			self::API_SHOWCASE . $showcaseId . self::API_VIDEO . '?' . $query
		);

		return $this->preprocessApiResponse($response);
	}
}
