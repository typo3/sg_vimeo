<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgVimeo\EventListeners;

use SGalinski\SgVimeo\Event\AfterFilterVideosEvent;

class AfterFilterVideosEventListener {
	public function __invoke(AfterFilterVideosEvent $event): void {
		// Modify the response if needed
		$response = $event->getResponse();
		// Add some custom processing here
		// For example let's remove the extra 10 videos that we added in the other event
		if (count($response['items']) > 10) {
			array_splice($response['items'], 0, 10);
		}
		$event->setResponse($response);
	}
}
