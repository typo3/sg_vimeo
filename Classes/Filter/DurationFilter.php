<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgVimeo\Filter;

use Exception;
use SGalinski\SgYoutube\Service\YoutubeService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DurationFilter implements FilterInterface {
	protected array $filterValues;
	protected array $filterConfig;
	private array $originalParameters;

	/**
	 * Constructor to initialize the filter with the query string.
	 *
	 * @param array $filterValues
	 * @param array $filterConfig
	 */
	public function __construct(array $filterValues, array $filterConfig) {
		$this->filterValues = $filterValues;
		$this->filterConfig = $filterConfig;
	}

	/**
	 * Modify the YouTube API request parameters (if necessary).
	 */
	public function modifyRequest(array &$parameters): void {
		$this->originalParameters = $parameters;
		$parameters['fields'] .= ',duration';
		$parameters['per_page'] = 100;
	}

	/**
	 * Optionally filter the API response, if needed.
	 * In this case, we won't filter the results, as we're modifying the query.
	 */
	public function modifyResponse(array &$data): void {
		$counter = 0;
		if (isset($this->filterValues['duration']) && !empty($this->filterValues['duration'])) {
			foreach ($data['items'] as $key => $item) {
				$counter++;

				if ($counter >= $this->originalParameters['per_page']) {
					break;
				}

				if ($this->filterValues['duration'] === "1") {
					if ($item['duration'] > 1 * 60) {
						unset($data['items'][$key]);
					}
				}

				if ($this->filterValues['duration'] === "2") {
					if ($item['duration'] <= 1 * 60) {
						unset($data['items'][$key]);
					}
				}
			}
		}
	}

	public function getFilterValues(): array {
		return $this->filterValues;
	}

	public function setFilterValues(array $filterValues): void {
		$this->filterValues = $filterValues;
	}

	/**
	 * Transforms the YouTube duration string into seconds
	 *
	 * @param $youtubeDuration
	 * @return float|int
	 */
	private function youtubeDurationToSeconds($youtubeDuration) {
		// Define a pattern to capture the ISO 8601 duration format
		$pattern = '/PT((\d+)H)?((\d+)M)?((\d+)S)?/';

		// Initialize hours, minutes, and seconds to zero
		$hours = 0;
		$minutes = 0;
		$seconds = 0;

		// Match the provided duration with the pattern
		if (preg_match($pattern, $youtubeDuration, $matches)) {
			// Check if hours are set and convert to integer
			if (!empty($matches[2])) {
				$hours = (int) $matches[2];
			}
			// Check if minutes are set and convert to integer
			if (!empty($matches[4])) {
				$minutes = (int) $matches[4];
			}
			// Check if seconds are set and convert to integer
			if (!empty($matches[6])) {
				$seconds = (int) $matches[6];
			}
		}

		// Convert the full duration to seconds
		return ($hours * 3600) + ($minutes * 60) + $seconds;
	}
}
