<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgVimeo\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\SgVimeo\Event\AfterFilterVideosEvent;
use SGalinski\SgVimeo\Event\AfterMapCustomThumbnailsEvent;
use SGalinski\SgVimeo\Event\AfterVimeoCallEvent;
use SGalinski\SgVimeo\Event\BeforeVimeoCallEvent;
use SGalinski\SgVimeo\Exception\RegionRestrictedException;
use SGalinski\SgVimeo\Filter\FilterParameterBag;
use SGalinski\SgVimeo\Service\CachedImageService;
use SGalinski\SgVimeo\Service\VimeoService;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Service\ImageService;

/**
 *  The Vimeo Controller
 */
class VimeoController extends ActionController {
	/**
	 * @var FrontendInterface
	 */
	protected $cache;

	/**
	 * @param FrontendInterface $cache
	 */
	public function injectCache(FrontendInterface $cache) {
		$this->cache = $cache;
	}

	/**
	 * Renders the Vimeo video view
	 *
	 * @return ResponseInterface
	 */
	public function indexAction(): ResponseInterface {
		$id = $this->settings['id'] ?? '';
		$filterIds = $this->settings['filterId'] ?? '';
		$maxResults = $this->settings['maxResults'] ?? 10;
		$clientId = $this->settings['clientId'] ?? '';
		$clientSecret = $this->settings['clientSecret'] ?? '';
		$personalAccessToken = $this->settings['personalAccessToken'] ?? '';
		$queryString = $this->settings['queryString'] ?? '';

		if (trim($clientId) === '' || trim($clientSecret) === '') {
			$this->view->assign('error', 'Please configure a client id and a client secret for sg_vimeo.');
			return $this->htmlResponse();
		}

		$vimeoParameters = [
			'id' => $id,
			'queryString' => $queryString,
		];

		$filterIds = GeneralUtility::trimExplode(',', $filterIds);

		// Get input values
		$elementId = $this->request->getAttribute('currentContentObject')->data['uid'];
		$inputValues = $this->request->getQueryParams();
		$filterValues = [];
		foreach ($inputValues as $key => $value) {
			if (str_starts_with($key, $elementId . '__')) {
				[$elementUid, $filterName, $inputName] = explode('__', $key);
				if ((int) $elementUid === $elementId) {
					$filterValues[$filterName][$inputName] = $value;
				}
			}
		}

		$typo3Version = VersionNumberUtility::getCurrentTypo3Version();
		if (version_compare($typo3Version, '13.0.0', '<')) {
			$disableVimeoCache = (bool) GeneralUtility::_GP('disableVimeoCache');
		} else {
			$disableVimeoCache = (bool) ($this->request->getParsedBody()['disableVimeoCache'] ?? $this->request->getQueryParams()['disableVimeoCache'] ?? NULL);
		}

		// Add third-party filters
		$filterInstances = $this->handleFrontendFilters($filterValues);

		$maxResultsWithFilters = (string) ((int) $maxResults + count($filterIds));
		$vimeoParameters['maxResults'] = $maxResultsWithFilters;

		// add Filter Values
		$vimeoParameters['filterValues'] = $filterValues;

		$filterParameterBag = new FilterParameterBag($vimeoParameters, $filterInstances);

		$vimeoService = GeneralUtility::makeInstance(
			VimeoService::class,
			$clientId,
			$clientSecret,
			$personalAccessToken,
			$this->cache
		);
		try {
			// Dispatch the BeforeYoutubeCallEvent
			$beforeVimeoCallEvent = new BeforeVimeoCallEvent($filterParameterBag);
			$this->eventDispatcher->dispatch($beforeVimeoCallEvent);

			// Use the possibly modified parameters
			try {
				$response = $vimeoService->getVimeoData($filterParameterBag);
			} catch (RegionRestrictedException $exception) {
				$response = [];
				$response['items'] = [];
				$response['kind'] = 'channel';
				$this->view->assign('regionRestricted', TRUE);
			}

			if (count($response['items']) < 1) {
				$this->view->assignMultiple([
					'notFound' => TRUE,
					'pluginContentData' => $this->request->getAttribute('currentContentObject')->data
				]);
			}

			if ($response === NULL) {
				return $this->htmlResponse();
			}
		} catch (\Exception $exception) {
			$this->view->assign('error', $exception->getMessage());
			return $this->htmlResponse();
		}

		// Dispatch the AfterVimeoCallEvent
		$afterVimeoCallEvent = new AfterVimeoCallEvent($response);
		$this->eventDispatcher->dispatch($afterVimeoCallEvent);

		// Use the possibly modified response
		$response = $afterVimeoCallEvent->getResponse();

		if (is_array($response['items'])) {
			$response['items'] = array_filter($response['items'], function ($item) use ($filterIds) {
				$videoId = $item['videoId'] ?? '';
				return !in_array($videoId, $filterIds, TRUE);
			});

			// Fix the array indexes from previous filtering
			$response['items'] = array_values($response['items']);
			while (count($response['items']) > $maxResults) {
				array_pop($response['items']);
			}

			// Dispatch the AfterFilterVideosEvent
			$afterFilterVideosEvent = new AfterFilterVideosEvent($response);
			$this->eventDispatcher->dispatch($afterFilterVideosEvent);

			// Use the possibly modified response
			$response = $afterFilterVideosEvent->getResponse();

			$response = $this->mapVimeoApiResponseWithPossibleCustomThumbnails($response);

			// Dispatch the AfterMapCustomThumbnailsEvent
			$afterMapCustomThumbnailsEvent = new AfterMapCustomThumbnailsEvent($response);
			$this->eventDispatcher->dispatch($afterMapCustomThumbnailsEvent);

			// Use the possibly modified response
			$response = $afterMapCustomThumbnailsEvent->getResponse();

			foreach ($response['items'] as &$item) {
				/*
				 * Check if link is of the form "https://vimeo.com/123456789", not "https://vimeo.com/username/videoname".
				 * Just checks if the end of `link` is the same as `videoId`, and if not, it replaces `link`.
				 * This is needed as a workaround so that glightbox can detect the video as a vimeo video correctly in the frontend.
				 */
				$this->fixVideoLink($item);
			}

			unset($item);
		}

		$cachedImageService = GeneralUtility::makeInstance(CachedImageService::class, 'vimeo');
		foreach ($response['items'] as $key => $item) {
			if (!isset($item['thumbnail'])) {
				$thumbnailSource = $item['pictures']['base_link'] . '_' . $item['width'] . 'x' . $item['height'] . '?r=pad';
				$response['items'][$key]['thumbnail'] = $cachedImageService->getImage($thumbnailSource);
			}
		}

		$this->view->assignMultiple(
			[
				'response' => $response,
				'showApiResult' => (int) ($this->settings['showApiResult'] ?? 1),
				'showTitle' => (int) ($this->settings['showTitle'] ?? 1),
				'showDescription' => (int) ($this->settings['showDescription'] ?? 1),
				'pluginContentData' => $this->request->getAttribute('currentContentObject')->data
			]
		);

		return $this->htmlResponse();
	}

	/**
	 * Maps the given vimeo API response thumbnails with the possible custom ones from the plugin settings.
	 *
	 * @param array|null $response
	 * @return array|null
	 */
	protected function mapVimeoApiResponseWithPossibleCustomThumbnails(?array $response): ?array {
		if (!is_array($response) || !array_key_exists('items', $response)) {
			return NULL;
		}

		if (isset($response['items'][0]['error'])) {
			$message = $response['items'][0]['error'];
			if (isset($response['items'][0]['developer_message'])) {
				$message .= ' (' . $response['items'][0]['developer_message'] . ')';
			}

			throw new \RuntimeException($message);
		}

		$contentElementUid = $this->request->getAttribute('currentContentObject')->data['uid'] ?? 0;
		if ($contentElementUid <= 0) {
			return $response;
		}

		/** @var FileRepository $fileRepository */
		$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
		$fileObjects = $fileRepository->findByRelation(
			'tt_content',
			'settings.thumbnailImages',
			$contentElementUid
		);

		if (count($fileObjects) <= 0) {
			return $response;
		}

		/** @var FileReference $fileObject */
		foreach ($fileObjects as $index => $fileObject) {
			if (!isset($response['items'][$index])) {
				break;
			}

			$cropString = '';
			if ($fileObject->hasProperty('crop') && $fileObject->getProperty('crop')) {
				$cropString = $fileObject->getProperty('crop');
			}

			$cropVariantCollection = CropVariantCollection::create((string) $cropString);
			$cropArea = $cropVariantCollection->getCropArea();
			$processingInstructions = [
				'crop' => $cropArea->isEmpty() ? NULL : $cropArea->makeAbsoluteBasedOnFile($fileObject),
			];
			/** @var ImageService $imageService */
			$imageService = GeneralUtility::makeInstance(ImageService::class);
			$processedImage = $imageService->applyProcessingInstructions($fileObject, $processingInstructions);
			$response['items'][$index]['thumbnail'] = $imageService->getImageUri($processedImage);
			$response['items'][$index]['thumbnailImageObject'] = $fileObject;
		}

		return $response;
	}

	/**
	 * Fixes the embed Link format
	 *
	 * @param array $item
	 * @return void
	 */
	private function fixVideoLink(array &$item): void {
		$endOfLink = substr($item['link'], -strlen($item['videoId']));
		if (!((int) $endOfLink === $item['videoId'])) {
			$item['link'] = strstr($item['link'], '.com/', TRUE) . '.com/' . $item['videoId'];
		}

		if (isset($item['embed']['html'])) {
			$embedLink = [];
			preg_match('/src="([^"]+)"/i', $item['embed']['html'], $embedLink);
			$item['embedLink'] = html_entity_decode($embedLink[1]);
		}
	}

	/**
	 * Applies the third-party filters
	 *
	 * @param array $filterValues
	 * @return array
	 */
	private function handleFrontendFilters(array $filterValues): array {
		if (!isset($this->settings['selectedFilters']) || !$this->settings['selectedFilters']) {
			return [];
		}

		$filters = $this->settings['filters'] ?? [];
		$selectedFilters = GeneralUtility::trimExplode(',', $this->settings['selectedFilters']);
		$filterDataTop = [];
		$filterDataBottom = [];
		$filterInstances = [];

		// Categorize filters by position
		foreach ($filters as $filterName => $filterConfig) {
			if (!in_array($filterName, $selectedFilters)) {
				continue;
			}
			$partialName = $filterConfig['partial'] ?? '';
			$label = $filterConfig['label'] ?? ucfirst($filterName);
			$position = $filterConfig['position'] ?? 'top'; // Default to top

			$filterData = [
				'partial' => $partialName,
				'label' => $label,
				'name' => $filterName,
				'options' => $filterConfig['options'] ?? [],
			];

			if (isset($filterConfig['defaultValues'])) {
				$filterData['defaultValues'] = $filterConfig['defaultValues'];

				foreach ($filterData['defaultValues'] as $input => $defaultValue) {
					if (!isset($filterValues[$filterName][$input])) {
						$filterValues[$filterName][$input] = $defaultValue;
					}
				}
			}

			if ($position === 'bottom') {
				$filterDataBottom[] = $filterData;
			} else {
				$filterDataTop[] = $filterData;
			}

			if (class_exists($filterConfig['filterClass'])) {
				$specificFilterValues = $filterValues[$filterName] ?? [];
				$filterInstance = GeneralUtility::makeInstance(
					$filterConfig['filterClass'],
					$specificFilterValues,
					$filterConfig
				);

				// Register filter to modify the request before API call
				$filterInstance->setFilterValues($specificFilterValues);
				$filterInstances[] = $filterInstance;
			}
		}

		// Get submit button settings
		$submitButton = $this->settings['submitButton'] ?? [];

		// Assign filters to view for rendering
		$this->view->assign('filtersTop', $filterDataTop);
		$this->view->assign('filtersBottom', $filterDataBottom);
		$this->view->assign('filtersCount', count($filters));
		$this->view->assign('submitButton', $submitButton);
		$this->view->assign('filterValues', $filterValues);
		return $filterInstances;
	}
}
