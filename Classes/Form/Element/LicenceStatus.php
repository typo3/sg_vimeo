<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgVimeo\Form\Element;

use SGalinski\SgVimeo\Service\LicenceCheckService;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;

/**
 * Licence Status Field
 */
class LicenceStatus extends AbstractFormElement {
	public function render(): array {
		$resultArray = [];
		$responseData = $this->checkLicenceKey();
		if (!$responseData) {
			return [];
		}

		switch ($responseData['error']) {
			case 1:
				$errorOrWarning = 'danger';
				break;
			case 2:
				$errorOrWarning = 'warning';
				break;
			default:
				$errorOrWarning = 'success';
		}

		$message = '<div class="alert alert-' . $errorOrWarning . '" role="'
			. $errorOrWarning
			. '">' . $responseData['message'] . '</div>';
		$resultArray['html'] = $message;
		return $resultArray;
	}

	/**
	 * Checks the license key and retrieves license check response data.
	 *
	 * This function verifies if the current TYPO3 version is supported,
	 * updates the last AJAX notification check timestamp, and retrieves
	 * the license check response data.
	 *
	 * @return array An array containing license check response data,
	 *               or an empty array if the TYPO3 version is not supported.
	 */
	private function checkLicenceKey() {
		if (!LicenceCheckService::isTYPO3VersionSupported()
		) {
			return [];
		}

		LicenceCheckService::setLastAjaxNotificationCheckTimestamp();
		return LicenceCheckService::getLicenseCheckResponseData(TRUE);
	}
}
