<?php

/**
 * Important! Do not return a variable named $icons, because it will result in an error.
 * The core requires this file and then the variable names will clash.
 * Either use a closure here, or do not call your variable $icons.
 */

$iconList = [];
foreach (['extension-sg_vimeo' => 'sg-vimeo.png'] as $identifier => $path) {
	$iconList[$identifier] = [
		'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
		'source' => 'EXT:sg_vimeo/Resources/Public/Icons/' . $path,
	];
}

return $iconList;
