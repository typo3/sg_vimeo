<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use SGalinski\SgVimeo\Controller\VimeoController;
use SGalinski\SgVimeo\Event\AfterFilterVideosEvent;
use SGalinski\SgVimeo\Event\AfterMapCustomThumbnailsEvent;
use SGalinski\SgVimeo\Event\AfterVimeoCallEvent;
use SGalinski\SgVimeo\Event\BeforeVimeoCallEvent;
use SGalinski\SgVimeo\EventListeners\AfterBackendPageRenderEventListener;
use SGalinski\SgVimeo\EventListeners\AfterFilterVideosEventListener;
use SGalinski\SgVimeo\EventListeners\AfterMapCustomThumbnailsEventListener;
use SGalinski\SgVimeo\EventListeners\AfterVimeoCallEventListener;
use SGalinski\SgVimeo\EventListeners\BeforeVimeoCallEventListener;
use SGalinski\SgVimeo\EventListeners\PageContentPreviewRenderingEventListener;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Backend\Controller\Event\AfterBackendPageRenderEvent;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();
	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();
	$services->load('SGalinski\\SgVimeo\\', __DIR__ . '/../Classes/');
	$services->set('cache.sgvimeo_cache')
		->class(FrontendInterface::class)
		->factory([service(CacheManager::class), 'getCache'])
		->args(['sgvimeo_cache']);
	$services->set(VimeoController::class)
		->call('injectCache', ['$cache' => service('cache.sgvimeo_cache')]);
	$services->set(PageContentPreviewRenderingEventListener::class)
		->tag('event.listener', ['event' => PageContentPreviewRenderingEvent::class]);
	$services->set(AfterBackendPageRenderEventListener::class)
		->tag('event.listener', ['event' => AfterBackendPageRenderEvent::class]);

	// Uncomment the following code to try out the example EventListeners
	/*
	$services->set(BeforeVimeoCallEventListener::class)
		->tag('event.listener', ['event' => BeforeVimeoCallEvent::class]);
	$services->set(AfterVimeoCallEventListener::class)
		->tag('event.listener', ['event' => AfterVimeoCallEvent::class]);
	$services->set(AfterFilterVideosEventListener::class)
		->tag('event.listener', ['event' => AfterFilterVideosEvent::class]);
	$services->set(AfterMapCustomThumbnailsEventListener::class)
		->tag('event.listener', ['event' => AfterMapCustomThumbnailsEvent::class]);
	*/
};
