<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

$pluginSignature = ExtensionUtility::registerPlugin(
	'SgVimeo',
	'Vimeo',
	'Vimeo Videos',
	'extension-sg_vimeo',
	'plugins',
	'LLL:EXT:sg_vimeo/Resources/Private/Language/locallang.xlf:vimeoPluginDescription'
);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
ExtensionManagementUtility::addPiFlexFormValue(
	$pluginSignature,
	'FILE:EXT:sg_vimeo/Configuration/FlexForms/flexform_sgvimeo_vimeo.xml'
);
