<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addStaticFile(
	'sg_vimeo',
	'Configuration/TypoScript',
	'SG Vimeo'
);
