<?php

use SGalinski\SgVimeo\Backend\Ajax;

return [
	'sg_vimeo::checkLicense' => [
		'path' => '/sg_vimeo/checkLicense',
		'target' => Ajax::class . '::checkLicense',
	],
];
