<?php

use SGalinski\SgVimeo\Controller\VimeoController;
use SGalinski\SgVimeo\Form\Element\ChannelWarning;
use SGalinski\SgVimeo\Form\Element\LicenceStatus;
use SGalinski\SgVimeo\Hooks\LicenceCheckHook;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

if ($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_vimeo']['uncached'] ?? FALSE) {
	// Uncached version
	ExtensionUtility::configurePlugin(
		'SgVimeo',
		'Vimeo',
		[
			VimeoController::class => 'index',
		],
		[
			VimeoController::class => 'index',
		]
	);
} else {
	// Cached version
	ExtensionUtility::configurePlugin(
		'SgVimeo',
		'Vimeo',
		[
			VimeoController::class => 'index',
		]
	);
}

$currentTypo3Version = VersionNumberUtility::getCurrentTypo3Version();
if (version_compare($currentTypo3Version, '13.0.0', '<')) {
	// include Plugin sg_vimeo
	ExtensionManagementUtility::addPageTSConfig(
		'@import "EXT:sg_vimeo/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig"'
	);
}

// Caching
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['sgvimeo_cache'] ??= [];

// Add licenceCheck RenderType
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][] = [
	'nodeName' => 'SgVimeoLicenceCheck',
	'priority' => 40,
	'class' => LicenceStatus::class,
];

// Add Channel Warning render type
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][] = [
	'nodeName' => 'SgVimeoChannelWarning',
	'priority' => 40,
	'class' => ChannelWarning::class,
];
