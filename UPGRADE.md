## Version 4

- Dropped TYPO3 10 and 11 support
- Dropped PHP 7 support

## Version 3

- Requires a license key from now on
- Removed dependencies to project_theme
- Removed dependencies to project_theme_lightbox
- TYPO3 12 support

# Version 1.X.X to Version 2.0.0

`vimeoLightbox.js` (deprecated since 1.1.0) using magnific-popup and jQuery removed in favor of `sgVimeoLightbox.js` (
vanilla JS).
Extension project_theme_lightbox required starting with version 2.0.0.

- Dropped TYPO3 9 Support
- Dropped php 7.3 Support

## Version 1.1  ```project_theme_lightbox``` integration

- The magnific popup integration is deprecated and will be removed in later versions.
- Implement JavaScript according to the readme after integrating ```project_theme_lightbox```
